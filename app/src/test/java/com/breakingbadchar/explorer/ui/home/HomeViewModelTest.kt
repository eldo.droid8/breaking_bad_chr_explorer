package com.breakingbadchar.explorer.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.breakingbadchar.explorer.data.datasource.network.NetworkResult
import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.data.model.ErrorResponse
import com.breakingbadchar.explorer.data.repository.BreakingBadCharacterRepository
import com.breakingbadchar.explorer.ui.ViewState
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
internal class HomeViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    private lateinit var repository: BreakingBadCharacterRepository

    private lateinit var viewModel: HomeViewModel

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    val char = Character(
        birthday = "sadsa",
        appearance = listOf(1, 2, 3),
        category = "sad",
        img = "https://images.amcnetworks.com/amc.com/wp-content/uploads/2015/04/cast_bb_700x1000_walter-white-lg.jpg",
        name = "sadsa",
        nickname = "sfsaf",
        occupation = listOf("wqe", "qwew"),
        betterCallSaulAppearance = listOf(5, 3, 2),
        charId = 5,
        portrayed = "sda",
        status = "sda"
    )
    val characters = mutableListOf<Character>(char)
    val errorResponse = ErrorResponse(status_message = "Test error")
    private val networkResultSuccess = NetworkResult.ApiCallSuccess<List<Character>>(characters)

    private val networkResultError = NetworkResult.ApiCallError(errorResponse)

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun Given_Repository_Get_Char_When_Get_Bad_Char_Then_Api_Success() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
            coEvery {
                repository.getBadChars()
            } returns networkResultSuccess
            viewModel = HomeViewModel(repository)
            viewModel.stateNav.observeForever {}
            viewModel.observableList.value?.let {
                assert(it.isNotEmpty())
            }
        }

    @Test
    fun Given_Repository_Get_Char_When_Get_Bad_Char_Then_Api_Error() =
        coroutinesTestRule.testDispatcher.runBlockingTest {
           coEvery {
                repository.getBadChars()
            } returns networkResultError
            viewModel = HomeViewModel(repository)
            viewModel.stateNav.observeForever {}
            assert(viewModel.stateNav.value == ViewState.Failure(errorResponse))
        }
}