package com.breakingbadchar.explorer.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.navArgs
import com.breakingbadchar.explorer.BR
import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.databinding.FragmentBadCharDetailBinding


class BadCharDetailFragment : Fragment() {
    val args: BadCharDetailFragmentArgs by navArgs()
    var character: Character ?= null
    val charDetailViewModel: BadcharDetailViewModel by viewModels()
    lateinit var homeDetailBinding: FragmentBadCharDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeDetailBinding = FragmentBadCharDetailBinding.inflate(inflater, container, false)
        character = args.badCharSelected
        character?.let {
            charDetailViewModel.showCharacterDetails(it)
        }
        return homeDetailBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeDetailBinding.setVariable(BR.viewModel, charDetailViewModel)
        homeDetailBinding.executePendingBindings()
    }


}