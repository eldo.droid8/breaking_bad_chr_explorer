package com.breakingbadchar.explorer.ui.widgets

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.RecyclerView
import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.ui.home.HomeBadCharAdapter
import com.breakingbadchar.explorer.ui.home.RecyclerviewItemSelected
import com.bumptech.glide.Glide


@BindingAdapter("homeAdapter","itemClickListener", requireAll = false)
fun addDataToCharacterList(
    recyclerView: RecyclerView,
    homeData: MutableList<Character>?,
    clickListener: RecyclerviewItemSelected
) {
    try {
        homeData?.let {
            val adapter =
                    HomeBadCharAdapter()
            recyclerView.adapter = adapter
            adapter.setItemClickListener(clickListener)
            adapter.addRecylerViewListData(it)
        }
    } catch (e: ClassCastException) {
        e.printStackTrace()
    }
}

@BindingAdapter("imageUrl", "placeholderData", requireAll = false)
fun setImageUrl(imageView: ImageView, url: String?, placeholderIcon: Drawable) {
    val context = imageView.context
    Glide.with(context).load(url)
        .centerInside()
        .placeholder(placeholderIcon)
        .into(imageView)
}

