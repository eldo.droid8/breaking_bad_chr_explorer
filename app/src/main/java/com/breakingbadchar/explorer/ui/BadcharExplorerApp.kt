package com.breakingbadchar.explorer.ui

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class BadcharExplorerApp: Application() {
    override fun onCreate() {
        super.onCreate()
    }
}