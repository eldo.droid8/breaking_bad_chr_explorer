package com.breakingbadchar.explorer.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.breakingbadchar.explorer.data.datasource.network.NetworkResult
import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.data.repository.BreakingBadCharacterRepository
import com.breakingbadchar.explorer.ui.ViewState
import com.breakingbadchar.explorer.ui.widgets.SingleLiveEvent
import kotlinx.coroutines.launch


class HomeViewModel @ViewModelInject constructor(
        private val repository: BreakingBadCharacterRepository
) : ViewModel() {
    val homeNavEvents: SingleLiveEvent<HomeNavigation> = SingleLiveEvent()
    val stateHomeEvents: LiveData<HomeNavigation> = homeNavEvents
    val characterList: MutableLiveData<List<Character>> = MutableLiveData()
    val observableList: LiveData<List<Character>> = characterList
    val showShimmerAnimation = MutableLiveData(true)
    val itemSelected: RecyclerviewItemSelected
        get() = this::characterSelected
    private val characterLiveData: MutableLiveData<ViewState<List<Character>>> = MutableLiveData()

    val stateNav: LiveData<ViewState<List<Character>>> = characterLiveData

    init {
        getBadCharacter()
    }

    private fun getBadCharacter() {
        viewModelScope.launch {
            adjustShimmerVisibility(true)
            when (val result = repository.getBadChars()) {
                is NetworkResult.ApiCallSuccess -> {
                    adjustShimmerVisibility(false)
                    result?.data?.let {
                        characterLiveData.value = ViewState.Success(it)
                        characterList.apply {
                            value = it
                        }
                    }
                }
                is NetworkResult.ApiCallError -> {
                    adjustShimmerVisibility(false)
                    characterLiveData.value = ViewState.Failure(result.error)
                }
                else -> {
                    adjustShimmerVisibility(false)
                }
            }
        }
    }

    fun showFilterDialog() {
        homeNavEvents.value = HomeNavigation.openFilterDialog
    }

    private fun adjustShimmerVisibility(visibility: Boolean) {
        showShimmerAnimation.value = visibility
    }

    private fun characterSelected(character: Character) {
        homeNavEvents.value = HomeNavigation.NavigateToDetail(character)
    }
}

sealed class HomeNavigation {
    data class NavigateToDetail(val character: Character) : HomeNavigation()
    object openFilterDialog : HomeNavigation()
}