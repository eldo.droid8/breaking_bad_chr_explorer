package com.breakingbadchar.explorer.ui.home

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.breakingbadchar.explorer.data.model.Character

class HomeItemViewModel(val character: Character,val itemSelected: RecyclerviewItemSelected?) {
    val imageUrl: LiveData<String> = MutableLiveData(character.img)
    val label: LiveData<String> = MutableLiveData(character.name)

    fun onCharacterSelected() {
        itemSelected?.invoke(character)
    }

}