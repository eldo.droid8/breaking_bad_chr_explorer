package com.breakingbadchar.explorer.ui

import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.data.model.ErrorResponse

sealed class ViewState<out T: Any> {

    data class Loading(val isLoading: Boolean) : ViewState<Nothing>()
    data class Success<out T : Any>(val output: T) : ViewState<T>()
    data class Navigate<out T : Any>(val output: T) : ViewState<T>()
    data class Failure(val throwable: ErrorResponse) : ViewState<Nothing>()
}



