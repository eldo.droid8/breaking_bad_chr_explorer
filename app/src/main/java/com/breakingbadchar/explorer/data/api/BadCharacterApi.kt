package com.breakingbadchar.explorer.data.api

import com.breakingbadchar.explorer.data.model.Character
import retrofit2.Response
import retrofit2.http.GET

interface BadCharacterApi {
    @GET("characters")
    suspend fun getAllBadCharacters(): Response<List<Character>>

}