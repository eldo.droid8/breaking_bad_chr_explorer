package com.breakingbadchar.explorer.data.model

data class ErrorResponse(val status_code: Int = 0,
                         val status_message: String? = null)