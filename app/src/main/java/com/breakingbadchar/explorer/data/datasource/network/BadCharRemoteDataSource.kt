package com.breakingbadchar.explorer.data.datasource.network

import com.breakingbadchar.explorer.data.api.BadCharacterApi
import com.breakingbadchar.explorer.data.datasource.network.NetworkResult.*
import com.breakingbadchar.explorer.data.model.Character
import com.breakingbadchar.explorer.data.model.ErrorResponse
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException
import java.net.SocketException
import javax.inject.Inject

class BadCharRemoteDataSource @Inject constructor(val badCharacterApi: BadCharacterApi, val retrofit: Retrofit){

   suspend fun getBadCharacters(): NetworkResult<List<Character>> {
        return fetchResponse(request = {badCharacterApi.getAllBadCharacters()},
                            defaultErrorMessage = "Please check your network connection")
    }


    //we can move this method by creating a BaseDataSource and thus we can maintain its decoupled
    private suspend fun <T: Any> fetchResponse (request: suspend () -> Response<T>, defaultErrorMessage: String): NetworkResult<T> {
        return try {
            val result = request.invoke()
            return if (result.isSuccessful) {
                if (null == result.body()) {
                    ApiCallError (ErrorResponse(status_message = "Empty response"))
                } else {
                    ApiCallSuccess(result.body())
                }
            } else {
                val errorResponse = parseError(result)
                val apiCallError = ApiCallError(errorResponse
                        ?: ErrorResponse(status_message = defaultErrorMessage))
                apiCallError
            }
        } catch (e: IOException) {
            ApiCallError(ErrorResponse(status_message = "Unknown error"))
        }
        catch (e: SocketException){
            ApiCallError(ErrorResponse(status_message = "Please check your network connection"))
        }
    }

    fun parseError(response: Response<*>): ErrorResponse? {
        val converter = retrofit.responseBodyConverter<ErrorResponse>(ErrorResponse::class.java, arrayOfNulls(0))
        return try {
            converter.convert(response.errorBody()!!)
        } catch (e: IOException) {
            ErrorResponse()
        }
    }
}