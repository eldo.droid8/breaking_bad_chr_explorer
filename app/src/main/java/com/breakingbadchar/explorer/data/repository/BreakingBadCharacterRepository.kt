package com.breakingbadchar.explorer.data.repository

import com.breakingbadchar.explorer.data.datasource.network.BadCharRemoteDataSource
import com.breakingbadchar.explorer.data.datasource.network.NetworkResult
import com.breakingbadchar.explorer.data.model.Character
import javax.inject.Inject

class BreakingBadCharacterRepository @Inject constructor(val badCharRemoteDataSource: BadCharRemoteDataSource){
    suspend fun getBadChars(): NetworkResult<List<Character>> {
        return badCharRemoteDataSource.getBadCharacters()
    }
}