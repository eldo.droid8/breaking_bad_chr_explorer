package com.breakingbadchar.explorer.data.datasource.network

import com.breakingbadchar.explorer.data.model.ErrorResponse

sealed  class NetworkResult<out T : Any> {
    object Loading: NetworkResult<Nothing>()
    data class ApiCallSuccess<out T : Any>(val data: T?): NetworkResult<T>()
    data class ApiCallError(val error: ErrorResponse): NetworkResult<Nothing>()
}

