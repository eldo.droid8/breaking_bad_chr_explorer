package com.breakingbadchar.explorer.di.module

import com.breakingbadchar.explorer.BuildConfig
import com.breakingbadchar.explorer.data.api.BadCharacterApi
import com.breakingbadchar.explorer.data.util.NetworkConstants
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides

import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {
    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient,converterFactory: MoshiConverterFactory,
                        @Named("baseUrl") url: String): Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .client(okHttpClient)
            .addConverterFactory(converterFactory)
            .build()
    }

    @Provides
    @Singleton
    fun provideMoshi(): Moshi{
        return  Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    }

    @Provides
    @Singleton
    fun provideMoshiConverterFactory(moshi: Moshi): MoshiConverterFactory{
        return  MoshiConverterFactory.create(moshi)
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(): OkHttpClient {
        val httpClient = OkHttpClient().newBuilder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
            return httpClient.addInterceptor(interceptor).build()
        }
        return httpClient.build()
    }

    @Provides
    @Singleton
    fun provideBadCharacterApiService(retrofit: Retrofit): BadCharacterApi {
        return retrofit.create(BadCharacterApi::class.java)
    }

    @Provides
    @Named("baseUrl")
    @Singleton
    fun provideUrl(): String {
        //here we can switch between debug and production url
        return NetworkConstants.BREAKING_BAD_CHAR_ENDPOINT
    }
}